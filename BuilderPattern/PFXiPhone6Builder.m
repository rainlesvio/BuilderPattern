//
//  PFXiPhone6Builder.m
//  Test
//
//  Created by xueying on 3/2/15.
//  Copyright (c) 2015 wuxueying. All rights reserved.
//

#import "PFXiPhone6Builder.h"
#import "PFXiPhone6.h"

@implementation PFXiPhone6Builder

- (PFXiPhone6 *)build {
    NSAssert(self.place, @"place");
    
    PFXiPhone6 *iphone6 = [[PFXiPhone6 alloc] init];
    iphone6.storage = _storage;
    iphone6.place = _place;
    
    return iphone6;
}

@end
