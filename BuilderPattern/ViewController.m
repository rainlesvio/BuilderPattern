//
//  ViewController.m
//  Test
//
//  Created by xueying on 2/27/15.
//  Copyright (c) 2015 wuxueying. All rights reserved.
//

#import "ViewController.h"
#import "PFXiPhone6.h"
#import "PFXiPhone6Builder.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    PFXiPhone6 *iphone6 = [PFXiPhone6 createWithBuilder:^(PFXiPhone6Builder *builder) {
        builder.storage = 64;
        builder.place = @"place";
    }];
    
    NSLog(@"%d-%@",iphone6.storage,iphone6.place);
}

@end
