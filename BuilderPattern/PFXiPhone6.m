//
//  PFXiPhone6.m
//  Test
//
//  Created by xueying on 3/2/15.
//  Copyright (c) 2015 wuxueying. All rights reserved.
//

#import "PFXiPhone6.h"
#import "PFXiPhone6Builder.h"

@implementation PFXiPhone6

+ (instancetype)createWithBuilder:(BuilderBlock)block {
    NSParameterAssert(block);
    PFXiPhone6Builder *builder = [[PFXiPhone6Builder alloc] init];
    block(builder);
    return [builder build];
}

@end
