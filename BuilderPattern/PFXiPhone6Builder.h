//
//  PFXiPhone6Builder.h
//  Test
//
//  Created by xueying on 3/2/15.
//  Copyright (c) 2015 wuxueying. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFXiPhone6Builder : NSObject

@property (nonatomic, assign) NSInteger storage;
@property (nonatomic, strong) NSString *place;

- (id)build;

@end
