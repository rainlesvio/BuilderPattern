//
//  PFXiPhone6.h
//  Test
//
//  Created by xueying on 3/2/15.
//  Copyright (c) 2015 wuxueying. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PFXiPhone6Builder;
typedef void(^BuilderBlock) (id);
@interface PFXiPhone6 : NSObject

@property (nonatomic, assign) NSInteger storage;
@property (nonatomic, strong) NSString *place;

+ (instancetype)createWithBuilder:(BuilderBlock)block;

@end
